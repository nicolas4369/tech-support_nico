

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * Classe-test SupportSystemTest.
 *
 * @author  (votre nom)
 * @version (un numéro de version ou une date)
 *
 * Les classes-test sont documentées ici :
 * http://junit.sourceforge.net/javadoc/junit/framework/TestCase.html
 * et sont basées sur le document Š 2002 Robert A. Ballance intitulé
 * "JUnit: Unit Testing Framework".
 *
 * Les objets Test (et TestSuite) sont associés aux classes à tester
 * par la simple relation yyyTest (e.g. qu'un Test de la classe Name.java
 * se nommera NameTest.java); les deux se retrouvent dans le męme paquetage.
 * Les "engagements" (anglais : "fixture") forment un ensemble de conditions
 * qui sont vraies pour chaque méthode Test à exécuter.  Il peut y avoir
 * plus d'une méthode Test dans une classe Test; leur ensemble forme un
 * objet TestSuite.
 * BlueJ découvrira automatiquement (par introspection) les méthodes
 * Test de votre classe Test et générera la TestSuite conséquente.
 * Chaque appel d'une méthode Test sera précédé d'un appel de setUp(),
 * qui réalise les engagements, et suivi d'un appel à tearDown(), qui les
 * détruit.
 */
public class SupportSystemTest
{
    // Définissez ici les variables d'instance nécessaires à vos engagements;
    // Vous pouvez également les saisir automatiquement du présentoir
    // à l'aide du menu contextuel "Présentoir --> Engagements".
    // Notez cependant que ce dernier ne peut saisir les objets primitifs
    // du présentoir (les objets sans constructeur, comme int, float, etc.).
    private SupportSystem supportTester;

    /**
     * Constructeur de la classe-test SupportSystemTest
     */
    public SupportSystemTest()
    {
    }

    /**
     * Met en place les engagements.
     *
     * Méthode appelée avant chaque appel de méthode de test.
     */
    @Before
    public void setUp() // throws java.lang.Exception
    {
        // Initialisez ici vos engagements
        supportTester = new SupportSystem();
    }

    /**
     * Supprime les engagements
     *
     * Méthode appelée après chaque appel de méthode de test.
     */
    @After
    public void tearDown() // throws java.lang.Exception
    {
        //Libérez ici les ressources engagées par setUp()
    }

    @Test
    public void inputByeShouldReturnTrue()
    {
        assertEquals(true, supportTester.isFinished("bye"));
        // A new String object "bye" must return true
        String bye = new String("bye");
        assertEquals(true, supportTester.isFinished(bye));
    }
    
    @Test
    public void inputStartsWithByeShouldReturnTrue()
    {
        assertEquals(true, supportTester.isFinished("bye   "));
        assertEquals(true, supportTester.isFinished("bye\n"));
        assertEquals(true, supportTester.isFinished("bye bye"));
        assertEquals(true, supportTester.isFinished("bye bye\n"));
    }
    
    @Test
    public void inputStartsWithSpacesByeShouldReturnTrue()
    {
        assertEquals(true, supportTester.isFinished("  bye"));
        assertEquals(true, supportTester.isFinished("\t\tbye"));
        assertEquals(true, supportTester.isFinished(" \n\t bye"));
    }
    
    @Test
    public void inputStartsWithUppercaseByeShouldReturnTrue()
    {
        assertEquals(true, supportTester.isFinished("Bye"));
        assertEquals(true, supportTester.isFinished("bYe"));
        assertEquals(true, supportTester.isFinished("bYE"));
        assertEquals(true, supportTester.isFinished("BYE"));
    }
    
    @Test
    public void inputNotByeShouldReturnFalse()
    {
        assertEquals(true, supportTester.isFinished("good bye"));
    }
    
    
}

