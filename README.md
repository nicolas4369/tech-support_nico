# Projet: tech-support

Auteurs: Michael Kölling and David J. Barnes
Traduction: Laurent Pierron

Ce projet fait partie du matériel pour le livre

   **Objects First with Java - A Practical Introduction using BlueJ
   Fifth edition
   David J. Barnes and Michael Kölling
   Pearson Education, 2012**

Il est expliqué dans le chapitre 5.

Une implémentation d'un programme dialoguant à la manière d'*ELIZA* (http://fr.wikipedia.org/wiki/ELIZA) utilisé pour fournir un support technique à des clients ; introduit l'utilisation de bibliothèque de classes en général et de certaines classes en particulier ; lecture et écriture de documentation.

- - -

## Préparation du projet

1. Créer une bifurcation (fork) du projet **tech-support** dans votre compte Bitbucket.
2. Ajouter votre binôme et `lcpierron` en administrateur du projet.
3. Cloner votre projet sur votre machine locale.

- - -

## Expression du besoin

Le responsable du support technique du logiciel *Dodgy*
souhaite que vous réalisiez un logiciel, qui permette à ses
clients de résoudre eux-mêmes les problèmes rencontrés avec le logiciel
*Dodgy*, comme le ferait un opérateur de son centre d'appel.

Voici un exemple de dialogue, qui a été enregistré dans le
centre d'appel technique de *DodgySoft* :

```
Welcome to the DodgySoft Technical Support System.

Please tell us about your problem.
We will assist you with any problem you might have.
Please type 'bye' to exit our system.
> My system crashes all the time.
Well, it never crashes on our system. It must have something
to do with your system. Tell me more about your configuration.
> I am using Ubuntu. Should your program run on Debian ?
Have you checked that you do not have a dll conflict?
> I really need help ! It's not working.
That's not a bug, it's a feature!
> I am using Windows 9. Should your program run on ?
This is a known bug to do with the Windows operating system. Please
report it to Microsoft. There is nothing we can do about this.
> Can I use BlueJ to debug your expensive software ?
Ahhh, BlueJ, yes. We tried to buy out those guys long ago, but
they simply won't sell... Stubborn people they are. Nothing we can
do about it, I'm afraid.
> You are not very helpful !
I need a bit more information on that.
> I have no more information.
Could you elaborate on that?
> Elaborate what  ?
That sounds odd. Could you describe that problem in more detail?
> Bye
Nice talking to you. Bye...
```

Les lignes de textes débutants par le symbole d'invite (*prompt*) sont écrits
par l'utilisateur demandant de l'aide, l'ordinateur doit créer les textes des
autres lignes.

## Conception

A priori, ce problème semble difficile à résoudre, car il ressemble au test de
Turing[^1] en intelligence artificielle (http://fr.wikipedia.org/wiki/Test_de_Turing).
Mais votre culture informatique vous permet de vous souvenir d'un système
similaire réalisé dans le logiciel **ELIZA** (http://fr.wikipedia.org/wiki/ELIZA)

Vous allez donc réaliser un logiciel similaire à **ELIZA**, spécialisé pour
ce contexte de support technique.

[^1]: Voir le film *Imitation Game* https://fr.wikipedia.org/wiki/Imitation_Game) sorti au cinéma en France le 28 janvier 2015.


### Identifier les objets

La première chose à faire dans un travail est d'**identifier les objets**
impliqués dans le projet pour en déduire des classes et des méthodes.

Les objets utilisés dans ce projet sont : les phrases données par l'utilisateur,
les réponses données par l'ordinateur.

Donc on aura deux classes :
  1. `InputReader` : dédiée à la lecture des questions de l'utilisateur ;
  2. `Responder` : dédié à la création des réponses.

Il faudra une troisième classe (`SupportSystem`) pour orchestrer la communication
entre les deux classes déduites du problème, il est assez fréquent d'avoir
des classes supplémentaires à celles déduites pour modéliser le problème posé,
cela fait partie de la mécanique interne du logiciel.

## Première version - mise en place du dialogue

Comme à l'accoutumé, vous allez réaliser votre logiciel de manière incrémentale.

La première version consistera à mettre en place le dialogue,
c'est celle que vous avez récupéré du serveur `BitBucket`, et que vous devez
améliorer pour arriver à la version finale.

### Discussion sur la boucle des évènements

La boucle `while` de la méthode `SupportSystem.start()` est souvent appelée **boucle des évènements**,
elle sera utilisée dans tous les programmes où l'on aura une interaction avec
l'environnement, duquel arriveront des évènements.

Dans le corps de la boucle le traitement est le suivant :
  1. Récupérer un évènement et ses données associées.
  2. Effectuer une action en fonction de l'évènement.

La boucle est sensée ne jamais se terminer, tant qu'elle ne reçoit pas un évènement
indiquant la fin du traitement.

Ce *patron* de conception est très fréquent puisqu'il intervient
dans tous les programmes interactifs (jeux, interface graphique, etc.)
mais aussi dans les programmes serveurs. Dans certains systèmes, la boucle peut être
masquée et le développeur doit juste associer les *évènements* avec
des *actions*.

Exercice : Trouvez plusieurs autres manières d'écrire cette boucle :

    - en utilisant une variable booléenne,
    - sans l'instruction `break`,
    - avec l'instruction `do {instruction (s)} while (expression);`,
    - en utilisant une fonction récursive

Quelle méthode préférez-vous ?

### Amélioration de `isFinished()`

1. Ouvrir le projet `tech-support`sous BlueJ.
1. Essayez le programme en créant un objet `SupportSystem` et en lançant `start()`.
1. Est-ce que vous pouvez quitter le dialogue ?
1. Pour sortir du dialogue : `Menu Outils -> Réinitialiser la machine`

Essayez de comprendre pourquoi vous ne pouvez pas sortir du dialogue, vous pouvez vous aider des étapes suivantes :

1. Lancez les tests, seul un passe sur les cinq.
1. Modifiez la méthode isFinished pour passer le premier test
1. Est-ce que maintenant vous pouvez quitter le dialogue ?
1. Enregistrez votre travail : `git commit -am "Correction du test d'égalité == de 'isFinished'"`
1. En vous aidant de la documentation de la classe String (http://docs.oracle.com/javase/8/docs/api/java/lang/String.html), modifiez `isFinished` pour passer les tests, *committez* après chaque test réussi.

### Ajout de réponses aléatoires

Actuellement le logiciel ne fournit qu'une seule réponse qu'elle que soit la question posée, c'est plutôt monotone.

Le directeur technique propose de fournir par défaut les réponses suivantes de manière aléatoire.

```
That sounds odd. Could you describe that problem in more detail?

No other customer has ever complained about this before.
What is your system configuration?

That sounds interesting. Tell me more...

I need a bit more information on that.

Have you checked that you do not have a dll conflict?

That is explained in the manual. Have you read the manual?

Your description is a bit wishy-washy. Have you got an expert
there with you who could describe this more precisely?"

That's not a bug, it's a feature!"

Could you elaborate on that?"
```

Vous devez compléter la classe `Responder` pour renvoyer une réponse aléatoire.

Vous allez utiliser les classes de bibliothèques :

   - `ArrayList<E>` : liste d'objets à accès direct, http://docs.oracle.com/javase/8/docs/api/java/util/ArrayList.html

   - `Random` : génération de nombres aléatoires,  http://docs.oracle.com/javase/8/docs/api/java/util/Random.html

Ces classes sont dans le package `java.util` : http://docs.oracle.com/javase/8/docs/api/java/util/package-summary.html

Les classes de la bibliothèque doivent être importées en début de fichier de
définition de classes par une instruction `import`, par exemple :

```java
import java.util.ArrayList
```

Les classes du package `java.lang` sont automatiquement importées par tous les programmes Java. La classe `String` fait partie du package `java.lang`.

Dans la suite, tout le détail des modifications, n'est pas donné, à vous de
compléter notamment le corps du constructeur et les autres méthodes.

Vous allez tout d'abord compléter la classe `Responder` avec la liste des réponses :

1. ajoutez un attribut `responses` de type `ArrayList<String>`
1. ajoutez une méthode `fillResponses()`, qui remplira la liste
avec les phrases données par le directeur technique.
1. modifiez la méthode `generateResponse()` pour retourner la dernière phrase
des réponses possibles
1. vérifiez avec la création d'objets BlueJ que la liste `responses` est conforme.
1. testez le dialogue.
1. enregistrez votre travail par un `commit`

Complétez le programme pour choisir une phrase au hasard :

1. ajoutez un attribut de nom `randomGenerator` de classe `Random`
1. modifiez la méthode `generateResponse` pour choisir une réponse au hasard
1. testez le fonctionnement de votre programme
1. enregistrez votre travail par un `commit`

  > Questions challenges : Pourquoi n'a-t-on pas de tests automatiques dans cette partie de programme,
  que manque-t-il pour des tests automatiques ? Comment tester des réponses aléatoires ? Cherchez sur Internet ?


### Ajout de réponses dépendant de la question

Dans les logiciels de dialogue complexes descendants d'**ELIZA**, une analyse
grammaticale de la phrase entrée est effectuée la transformant en un abre
grammaticale voire sémantique, à partir duquel est séléctionné un modèle
de réponses souvent sous forme d'arbre ou de graphe,
modèle qui sert à la génération de la phrase de réponse.

Les arbres et les graphes, au même titre que les listes et les tableaux,
sont des structures de données classiques en informatique pour modéliser
des problèmes comme ceux que l'on rencontre dans la recherche en informatique.

Vous allez simplifier la question de la sélection de phrases en choisissant
de sélectionner une réponse en fonction d'un mot-clé se trouvant
dans la question.

Le responsable technique propose les associations suivantes :

```
"crash",   "Well, it never crashes on our system. It must have something\n" +
           "to do with your system. Tell me more about your configuration."

"crashes", "Well, it never crashes on our system. It must have something\n" +
           "to do with your system. Tell me more about your configuration."

"slow",    "I think this has to do with your hardware. Upgrading your processor\n" +
           "should solve all performance problems. Have you got a problem with\n" +
           "our software?"

"performance", "Performance was quite adequate in all our tests. Are you running\n" +
               "any other processes in the background?"

"bug", "Well, you know, all software has some bugs. But our software engineers\n" +
       "are working very hard to fix them. Can you describe the problem a bit\n" +
       "further?"

"buggy", "Well, you know, all software has some bugs. But our software engineers\n" +
         "are working very hard to fix them. Can you describe the problem a bit\n" +
         "further?"

"windows", "This is a known bug to do with the Windows operating system. Please\n" +
           "report it to Microsoft. There is nothing we can do about this."

"mac", "This is a known bug to do with the Mac operating system. Please\n" +
       "report it to Apple. There is nothing we can do about this."

"expensive", "The cost of our product is quite competitive. Have you looked around\n" +
             "and really compared our features?"

"installation", "The installation is really quite straight forward. We have tons of\n" +
                "wizards that do all the work for you. Have you read the installation\n" +
                "instructions?"

"memory", "If you read the system requirements carefully, you will see that the\n" +
          "specified memory requirements are 1.5 giga byte. You really should\n" +
          "upgrade your memory. Anything else you want to know?"

"linux", "We take Linux support very seriously. But there are some problems.\n" +
         "Most have to do with incompatible glibc versions. Can you be a bit\n" +
         "more precise?"

"bluej", "Ahhh, BlueJ, yes. We tried to buy out those guys long ago, but\n" +
         "they simply won't sell... Stubborn people they are. Nothing we can\n" +
         "do about it, I'm afraid."

```

Vous devez compléter la classe `Responder`, pour ajouter les associations
de mots-clés avec les réponses, et la classe `InputReader` pour envoyer une liste
de mots-clés au lieu d'une chaîne de caractères.

Les associations seront représentées en Java par un tableau associatif, qui est
une structure de donnée classique en informatique et souvent implémenté
comme un type de base au même titre que les tableaux et les listes dans les
langages de programmation moderne (JavaScript, Python, PHP, Ruby, Julia, etc.).
Pour les différences entre les langages de programmation voir : http://en.wikipedia.org/wiki/Comparison_of_programming_languages_(mapping)

Pour extraire les mots-clés de la question, vous transformerez la question en
une liste de mots, qui sera mise dans un ensemble. Un ensemble d'objets est
une liste d'objets sans doublons, on utilise souvent les tableaux associatifs
pour représenter les ensembles car les clés des tableaux associatifs sont uniques.  

La classe pilote `SupportSystem` sera également impliquée par les modifications.

Pour ce travail vous allez utiliser les classes de bibliothèques suivantes :

   - `HashMap<K, V>` : tableau associatif, http://docs.oracle.com/javase/8/docs/api/java/util/HashMap.html

   - `HashSet<E>` : ensemble représenté par un tableau associatif, http://docs.oracle.com/javase/8/docs/api/java/util/HashSet.html

Ces classes sont dans le package `java.util` : http://docs.oracle.com/javase/8/docs/api/java/util/package-summary.html

Dans la suite, tout le détail des modifications, n'est pas donné, à vous de
compléter notamment le corps du constructeur et les autres méthodes.

Vous allez tout d'abord compléter la classe `Responder` :

1. ajoutez un attribut `responseMap` de type `HashMap<String, String>`
1. ajoutez une méthode `fillResponseMap()`, qui remplira le tableau associatif `responseMap`
avec les phrases données par le directeur technique
1. changez le nom de l'attribut `responses` précédent en `defaultResponses`
1. créez une méthode `pickDefaultResponse()`, dont le but est retourner une phrase
choisie au hasard dans `defaulResponses`
1. modifiez la méthode `generateResponse()`, elle doit prendre
en paramètre la question sous forme de mots-clés et retourner la réponse associée
ou une des réponses par défaut si il n'y a pas de mots-clés associés.
1. vérifiez avec la création d'objets BlueJ que la liste `responseMap` est conforme
à ce que vous attendez.
1. créez deux tests pour vérifier le comportement par défaut de la nouvelle version
de `generateResponse()` et le comportement pour un mot-clé existant.
1. passez les tests, utilisez un ensemble vide pour l'appel à `generateResponse()`
dans la méthode `start()`
1. enregistrez votre travail par un `commit`

Maintenant complétez la classe  `InputReader` pour transformer la question
de l'utilisateur en un ensemble de mots-clés : en une question

1. ajoutez une méthode `public HashSet<String> stringToSet(String s)`, qui transforme
une chaîne en un ensemble, la classe `String` a une méthode pour découper une chaîne
en un tableau, puis avec un parcours du tableau par une instruction `for` complétez l'ensemble.
1. Créez deux méthodes de tests pour votre nouvelle méthode.
1. modifiez la méthode `getInput()` pour qu'elle retourne l'ensemble
1. modifiez la méthode `isFinished()` pour qu'elle prenne l'ensemble des
mots-clés en paramètre
1. modifiez la méthode `start()` de `SupportSystem` pour prendre en compte
l'ensemble de smodifications
1. testez le fonctionnement de votre programme
1. enregistrez votre travail par un `commit`

### Touche finale

Afin de pouvoir rendre le logiciel indépendant de BlueJ, il faut ajouter
une méthode spéciale dans la classe principale afin que le programme
puisse être exécuté depuis un terminal avec la commande :

```bash
java SupportSystem
```

La méthode à ajouter dans la classe `SupportSystem` est la suivante :

```java
public static void main(String[] args) {
  SupportSystem eliza = new SupportSystem();
  eliza.start();
}
```

1. Ajoutez cette méthode dans votre programme.
1. Compilez et testez, lancez les jeux de test.
1. Enregistrez votre travail par un `commit`
1. Synchronisez votre travail avec le serveur Bitbucket par la commande : `git push --all`

---

## Idéees complémentaires

Vous pouvez améliorer ce logiciel de diverses manières, voici quelques idées :

  - traduction en français,
  - plusieurs réponses possibles pour chaque mot-clé,
  - utilisation de fichiers pour les listes de phrases,
  - choix de la langue,
  - utilisation de réponse à trou reprenant des mots de la question,
  - détection de type de phrase et adaptation de la réponse : déclarative,
  exclamative, interrogative, impérative.

- - -