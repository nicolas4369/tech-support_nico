/**
 * This class implements a technical support system. It is the top
 * level class in this project. The support system communicates via
 * text input/output in the text terminal.
 * 
 * This class uses an object of class InputReader to read input from
 * the user, and an object of class Responder to generate responses.
 * It contains a loop that repeatedly reads input and generates output
 * until the users wants to leave.
 * 
 * @author     
 * @version    0.1 (2011.07.31)
 */
//Ceci importe la classe Scanner du package java.util
import java.util.Scanner; 
//Ceci importe toutes les classes du package java.util
import java.util.*;
public class SupportSystem
{
    private InputReader reader;
    private Responder responder;

    /**
     * Creates a technical support system.
     */
    public SupportSystem()
    {
        reader = new InputReader();
        responder = new Responder();
    }

    /**
     * Start the technical support system. This will print a welcome
     * message and enter into a dialog with the user, until the user
     * ends the dialog.
     */
    public void start()
    {
        printWelcome();

        while(true) {
            String input = reader.getInput();

            if (isFinished(input)) {break;}

            String response = responder.generateResponse();
            System.out.println(response);

        }
        printGoodbye();
    }

    /**
     * Print a welcome message to the screen.
     */
    private void printWelcome()
    {
        System.out.println("Bienvenue dans DodgySoft Technical Support System.");
        System.out.println();
        System.out.println("Dites nous votre probleme ");
        System.out.println("Nous allons vous assister dans la resoltion de votre probleme.");
        System.out.println("écrivez exit pour sortir du chat.");
    }

    /**
     * Print a good-bye message to the screen.
     */
    private void printGoodbye()
    {
        System.out.println("Cela fu");
    }

    /**
     * Check if input text is dialog finishing message.
     *
     * @param  input   text to test
     * @return     true if input text is dialog finishing message, false otherwise
     */
    public boolean isFinished(String input)
    {
        // Insérez votre code ici
        input = input.toLowerCase();
        return input.contains("bye") || input.contains ("good bye") ;
        
    }

}
